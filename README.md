# hedge2vcs

Converts your HedgeDoc history to Git commits.

## Example

```sh
~/myrepo/notes$ ~/hedge2vcs/hedge2vcs.clj
Ensure you're in empty target directory inside a git repository (otherwise abort with Ctrl+C now)
Open HedgeDoc in Browser
Open Developer Tools with F12
Load another page (open note)
Right click a request to HedgeDoc (includes domain and session cookie)
Copy => Copy as cURL
Paste here
Press Return key
Press Ctrl+D
curl 'https://hedgedoc.example.com/some-resource [...] -H 'Cookie: [...] connect.sid=sessioncookie.....; [...]
2023-05-22T17:17:15.174Z host INFO [hedge2vcs:?] - https://hedgedoc.example.com/first-note/revision
2023-05-22T17:17:15.200Z host INFO [hedge2vcs:?] - https://hedgedoc.example.com/second-note/revision
2023-05-22T17:17:15.271Z host INFO [hedge2vcs:?] - https://hedgedoc.example.com/first-note/revision/1672527661111
2023-05-22T17:17:15.374Z host INFO [hedge2vcs:?] - https://hedgedoc.example.com/second-note/revision/1675296122222
2023-05-22T17:17:15.508Z host INFO [hedge2vcs:?] - https://hedgedoc.example.com/first-note/revision/1677805383333
[...]

~/myrepo/notes$ git log --format='%ai %s' .
[...]
2023-03-03 03:03:03 +0200 first-note 1677805383333
2023-02-02 02:02:02 +0200 second-note 1675296122222
2023-01-01 01:01:01 +0200 first-note 1672527661111
```

## Quickstart

[Babashka](https://babashka.org/) and a working Git setup are required.

Clone the repository and run `/path/to/hedge2vcs/hedge2vcs.clj`.

Alternatively you can download and execute the program with

```sh
bb <(curl https://gitlab.com/hxtmdev/hedge2vcs/-/raw/main/hedge2vcs.clj)
```

Make sure to read [the code](./hedge2vcs.clj) first!

## What's this?

This script converts the edit history of [HedgeDoc](https://hedgedoc.org/) notes to Git commits.
This can be useful if you don't just want to export the latest versions but the changes over time as well.

This is a rather quick-and-dirty script without automated tests.
Consider this "alpha quality" and use at your own risk (see [LICENSE](./LICENSE)).

This is an independent project and not affiliated with HedgeDoc or Git.

## What it can do

- Create Git commits for all revisions of HedgeDoc notes in chronological order.
- Preserve revision timestamp as author date.
- Save notes in files with their last HedgeDoc title as the filename.

## What it can't do (yet?)

- Save anything but the textual content (no images and attachments)
- Changing filenames
- Preserve author information (one revision can have multiple authors anyway)
- Incremental saves / resuming
- Elaborate error handling (apart from a delay and retry on HTTP errors it just crashes)

## Contributing

### Bugs

Browse and report bugs here: [gitlab.com/hxtmdev/hedge2vcs/-/issues](https://gitlab.com/hxtmdev/hedge2vcs/-/issues)

### Small changes

If you have a small improvement or bugfix that does not add much complexity just open a merge request here: [gitlab.com/hxtmdev/hedge2vcs/-/merge_requests](https://gitlab.com/hxtmdev/hedge2vcs/-/merge_requests)

### Big changes

If you intend to make bigger changes or spend lots of time on something _and want it integrated into this repository_, please open an issue first to discuss it.
Of course, you have the option to maintain your own fork in line with the [license](./LICENSE).

### Commit requirements

All commits require a [DCO](https://developercertificate.org/) sign-off, which can be generated automatically by [calling `git commit` with `--signoff`](https://git-scm.com/docs/git-commit#Documentation/git-commit.txt---signoff).

Please include a body in every commit message with any details you think can help others understand what you did and why.
