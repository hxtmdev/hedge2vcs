#!/usr/bin/env bb

(ns hedge2vcs
  (:gen-class)
  (:require [org.httpkit.client :as http]
            [clojure.tools.logging :refer [info error]]
            [cheshire.core :as json]
            [clojure.java.shell :as shell]
            [clojure.string :as str]
            [clojure.java.io :as io]))

(defn throw-rte [& s]
  (let [msg (str/join s)]
    (error msg)
    (throw (RuntimeException. msg))))

(defn git
  "Execute git, throwing on non-zero exit code."
  [& args]
  (let [str-args (mapv str (cons "git" args))
        result (apply shell/sh str-args)
        exit (:exit result)]
    (when-not (zero? exit)
      (throw-rte "non-zero exit" str-args result))))

(defn json-decode
  "Decode JSON string with keyword keys."
  [s]
  (json/decode s true))

(defn must-find
  "Try to find the first substring matching a regular expression.
   Throw exception if not found."
  [s r]
  (or (re-find r s)
      (throw-rte r "not found")))

(defn parse-infos [infos]
  {:url (must-find infos #"https?://[^/']+")
   :sid (must-find infos #"connect.sid=[^;']+")})

(defn make-dl
  "Create function that joins URL part with /, performs a HTTP GET request,
   and parses the result as JSON,
   continually retrying after 5 seconds if not HTTP 200."
  [{:keys [url sid]}]
  (fn [& parts]
    (-> (str/join "/" (cons url parts))
        (doto info)
        (#(loop []
            (let [res (deref (http/get % {:headers {"Cookie" sid}}))]
              (if (-> res :status #{200})
                res
                (do (Thread/sleep 5000)
                    (recur))))))
        :body
        json-decode)))

(defn with-filenames [notes]
  (let [determine-filename (fn [filenames note]
                             (->> (cons nil (range))
                                  (map #(str (-> note :text (str/replace #"^\.|/" "_"))
                                             %))
                                  (remove (-> filenames vals set))
                                  first
                                  (assoc filenames note)))
        filenames (->> notes
                       (sort-by :time)
                       (reduce determine-filename {}))]
    (->> notes
         (map #(assoc % :filename (filenames %))))))

(defn notes
  "Get list of note IDs."
  [dl]
  (let [notes (->> (dl "history")
                   :history
                   with-filenames)]
    (info "notes:" (->> notes
                        (map (juxt :id :filename))
                        (into {})))
    notes))

(defn revisions
  "Get revisions for a note."
  [dl {:keys [id filename]}]
  (->> (dl id "revision")
       :revision
       (map #(assoc %
                    :id id
                    :filename filename))))

(defn save-revision
  "Save revision of a note and create Git commit."
  [dl {:keys [id filename time]}]
  (info "saving" (str (java.util.Date. time)) filename)
  (spit (io/file filename)
        (:content (dl id "revision" time)))
  (git "add" "--" filename)
  (git "commit"
       "--allow-empty"
       "--no-verify"
       "--date" (quot time 1000)
       "-m" (str filename " " time)))

(defn -main
  "CLI entrypoint."
  []
  (run! println ["Ensure you're in empty target directory inside a git repository (otherwise abort with Ctrl+C now)"
                 "Open HedgeDoc in Browser"
                 "Open Developer Tools with F12"
                 "Load another page (open note)"
                 "Right click a request to HedgeDoc (includes domain and session cookie)"
                 "Copy => Copy as cURL"
                 "Paste here"
                 "Press Return key"
                 "Press Ctrl+D"])
  (let [infos (slurp *in*)
        dl (make-dl (parse-infos infos))]
    (when-not (->> (io/file ".")
                   .list
                   (remove #{".git"})
                   empty?)
      (throw-rte "Current directory not empty."))
    (->> (notes dl)
         (mapcat #(revisions dl %))
         (sort-by :time)
         (run! #(save-revision dl %)))))

; execute -main if run as a script
(when (= *file* (System/getProperty "babashka.file"))
  (apply -main *command-line-args*))