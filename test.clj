(ns test
  #_{:clj-kondo/ignore [:refer-all]}
  (:require [clojure.test :refer :all]
            [hedge2vcs :refer [with-filenames]]))

(deftest determine-filenames
  (are [notes result] (= result
                         (with-filenames notes))
    [] []
    
    [{:time 0 :text "..a/b."}] [{:time 0 :text "..a/b." :filename "_.a_b."}]

    [{:time 2 :text "x"}
     {:time 1 :text "x"}]
    [{:time 2 :text "x" :filename "x0"}
     {:time 1 :text "x" :filename "x"}]))

(let [summary (if-let [test (first *command-line-args*)]
                (run-test-var (find-var (symbol (str *ns*) test)))
                (run-tests))]
  (when-not (successful? summary)
    (System/exit 1)))
